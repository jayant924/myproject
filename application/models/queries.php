<?php
class queries extends CI_Model
{
    public function __construct() 
    {

       parent::__construct();

      $this->lang->load('message', 'english');
    
    }


   	public function getPosts(){
     
       $sql = $this->db->query("SELECT CONCAT(FIRSTNAME,' ', LASTNAME) AS Name, `email`,`reg_date`,`status`,`dateofbirth` FROM myguests")->result_array();
       
      
   		return $sql;

   	}


 function addtest($add_data=NULL)
    {
        $this->db->insert(TABLE_QUERIES, $add_data);
        // Return Insert id

        return $this->db->insert_id();
    }

function getdata($id=NULL)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get(TABLE_QUERIES)->row_array();
    }
    
     function updatedata($id=NULL, $update_data=NULL)
    {
        $this->db->where('id', $id);
        $this->db->update(TABLE_QUERIES, $update_data);
        //echo $this->db->last_query();
        return $id;
    }

  function deletedata($id=NULL)
    {
        $this->db->where('id', $id);
        $this->db->delete(TABLE_QUERIES);
         return $this->db->affected_rows();
    }

     


     public function check_login($user_name,$password){
      $sql = $this->db->get_where(TABLE_QUERIES,array('email'=>$user_name,'password'=>$password));
      return $sql->row_array();
     }
 
     public function getcountry(){
         $response = array();
      
      $this->db->select('*');
      $q = $this->db->get('countries');
      $response = $q->result_array();

      return $response;
     }

    
  
  public function getcity($postdata){
    $response = array();

    $this->db->select('id,name');
    $this->db->where('states', $postdata['states']);
    $q = $this->db->get('cities');
    $response = $q->result_array();

    return $response;

  }



// Call from Ajax function to get state according to country
  function get_state($country_id=NULL)
  {
    
    
    $this->db->where('country_id',$country_id);
    $query = $this->db->get(TABLE_STATE);
    $data_state=$query->result_array();
    
    $state_arr=array();
    
    if(!empty($data_state))
    {
      foreach($data_state as $x=>$state_val)
      {
        $state_arr[$state_val['id']]=$state_val['name'];
      }
    }
    
    //echo "<pre>"; print_r($state_arr); die;
    
    return $state_arr;
  }

  function get_city($state_id=NULL)
  {
    
    
    $this->db->where('state_id',$state_id);
    $query = $this->db->get(TABLE_CITY);
    $data_city=$query->result_array();
    
    $city_arr=array();
    
    if(!empty($data_city))
    {
      foreach($data_city as $x=>$city_val)
      {
        $city_arr[$city_val['id']]=$city_val['name'];
      }
    }
    
    //echo "<pre>"; print_r($state_arr); die;
    
    return $city_arr;
  }


public function record_count() {
return $this->db->count_all("myguests");
}





// Fetch data according to per_page limit.
public function fetch_data($limit=0, $id=0) {

  
$this->db->limit($limit);
$this->db->where('id', $id);
$query = $this->db->query("SELECT `id`,CONCAT(FIRSTNAME,' ', LASTNAME) AS Name, `email`,`reg_date`,`status`,`stage`,`dateofbirth` FROM myguests")->result_array();
       
      
     return  $query;


}








function pagination($total_rec, $current_page=0, $perPage, $url_name,$ajaxflag=NULL)
  {
    
    $pagination = "" ;
    $fromRec = "";
    $toRec = "";
    // if total records are less than per page record then dont show pagination
    if($total_rec<= $perPage)
    {
      if($total_rec>0)
      {
        $pagination .= '<div>Showing Records : 1 - '.$total_rec.' (Total '.$total_rec.')'.'</div>' ;
        return ($pagination);
      }
            else
      return ;
    }
    //Retrive Current Page number 
    if($current_page<0 || $current_page==0)
    {
      $p = 1 ;
    }
    else
    {
      $p = $current_page ;
    }

    //  Calculate Total Pages
    $total_pages = (int)(@$total_rec/@$perPage) ;
    if( ($total_rec%$perPage) > 0 )
    {
      $total_pages++ ;
    }
    
    //  To show pagination
    if($total_pages >= 1)
    {
      $pagination = '<div style="font-size: 17px;" class="table-default">
                      ';
      //  FIRST & PRIV LINKS
      if($p>1)
      {
        if($ajaxflag)
        {
          $pagination .= "<a class='page-link' href=javascript:ajaxPagination('".$url_name."/1')>".$this->lang->line("FIRST")."</a>&nbsp</li>;" ;
          $pagination .= "<a href=javascript:ajaxPagination('".$url_name."/". ($p-1) ."')>".$this->lang->line("PREV")."</a>&nbsp;" ;
          //$pagination .= "<a href=javascript:ajaxPagination('".$url_name."/". ($p-1) ."')>".lang("PREV")."</a>" ;
        }else
        {
          $pagination .= "<a href='".$url_name."/1'>".$this->lang->line("FIRST")."</a>&nbsp;" ;
          $pagination .= "<a href='".$url_name."/". ($p-1) ."'>".$this->lang->line("PREV")."</a>&nbsp;" ;
        }
      }
      


      // Middle links..
      if( ($p%BEFORE_AFTER_NO) == 0)
      {
        if( ($total_pages - $p) > BEFORE_AFTER_NO)
        {
          $start = $p-BEFORE_AFTER_NO ? $p-BEFORE_AFTER_NO:1 ;
        }
        
        else
        {
          $start = $total_pages - TOTAL_PAGINATION_NO;
          if($start <= 0)
          {
            $start = 1 ;
          } 
        } 
        
        $end = $p+BEFORE_AFTER_NO ;
      }
      else
      { 
        if($p > 9)
        {
          if( ($total_pages - $p) > BEFORE_AFTER_NO)
          {
            $start = $p - BEFORE_AFTER_NO ;
          }
          else
          {
            $start = $total_pages - TOTAL_PAGINATION_NO;
          }   
        $end = $p + BEFORE_AFTER_NO ;
        }
        else
        {  
        $start = 1 ;
        
        $end = TOTAL_PAGINATION_NO;
        
        } 
      } 
      for($i=$start; $i<=$end && $i<=$total_pages;$i++)
      {
        if($i==$p)
        {
          $pagination .= '<span><a class="page-item" id="current">'.$i.'</a></span> ' ;
        }
        
        else
        {
          if($ajaxflag)
            @$pagination .= "<a href=javascript:ajaxPagination('".$url_name."/". $i ."')>$i</a>&nbsp;" ;
          else
            $pagination .= "<a href='".$url_name."/". $i ."'>$i</a>&nbsp;" ;
        } 
      }
      //  NEXT & LAST links
      if($p>=1 && $p<$total_pages)
      {
        if($ajaxflag)
        {
          $pagination .= "<a href=javascript:ajaxPagination('".$url_name ."/". ($p+1) ."')>".$this->lang->line("NEXT")."</a>&nbsp;" ;
          $pagination .= "<a href=javascript:ajaxPagination('".$url_name ."/$total_pages')>".$this->lang->line("LAST")."</a>&nbsp;" ;
          //$pagination .= "<li><a href=javascript:ajaxPagination('".$url_name."/". ($p+1) ."')>".$this->lang->line("NEXT")."</a>&nbsp;</li>" ;
        } 
        else
        {
          $pagination .= "<a  href='".$url_name ."/". ($p+1) ."'>".$this->lang->line("NEXT")."</a>&nbsp;" ;
          $pagination .= "<a href='".$url_name ."/$total_pages'>".$this->lang->line("LAST")."</a>&nbsp;" ;
        }
      }
    } //  if($total_pages>1) end.
    
    $fromRec = ((@$p-1)*@$perPage+1);
    $toRec = (@$p*@$perPage);
    if($toRec>$total_rec)
    $toRec = $total_rec;
    
    $pagination .= '<br/><br/><h5 class="btn btn-primary btn-lg btn-block" style="height: 45px;
    padding-top: 5px;
    padding-left: 5px;">Showing Records :'.$fromRec.' - '.$toRec.' (Total '.$total_rec.')'.'</h5></div>' ;
    if($current_page>$total_pages)
    $pagination ='';
    
    return ($pagination);
  }

  function changeUserStatus($id,$status){
      $status = ($status==1)?0:1;
    $this->db->set('stage',$status); 
    $this->db->where('id',$id);
    $this->db->update(TABLE_QUERIES);
    return $this->basicDetails($id);    
    
    }

    //function to get name of the user
    function basicDetails($id){
      $this->db->select('firstname');
      $this->db->from(TABLE_QUERIES);
      return $this->db->where('id',$id)->get()->result_array();
      
    }

       
//function to get the list of active employees for case assign operation
    function getActiveEmployees(){
      $this->db->select('id,firstname');
      $this->db->from(TABLE_QUERIES);
      return $this->db->where('stage',1)->get()->result_array();
      
    }
// function to save case information into database
    function assignCase($data){
      return $this->db->insert('user_case',$data);
    }

    //function to get all records of case assign for assign list page
    //function to get all records of case assign for assign list page
    function getCases(){
     



$sql = $this->db->query("SELECT `e`.`id` AS `id`, `e`.`empid` AS `empid`, `a`.`name` AS `country`, `b`.`name` AS `state`, `c`.`name` AS `city`, `d`.`firstname`, DATE_FORMAT(e.created_at, \"%d-%m-%Y\") AS created_at, `email` FROM `user_case` `e` LEFT JOIN `states` `b` ON `b`.`id` = `e`.`state` LEFT JOIN `cities` `c` ON `c`.`id` = `e`.`city` LEFT JOIN `countries` `a` ON `a`.`id` = `e`.`country` LEFT JOIN `myguests` `d` ON `d`.`id` = `e`.`empid` GROUP BY `e`.`empid` ORDER BY `e`.`id`")->result_array();
       
      
      return $sql;




    }
    

    //function to get country,state and city with their ids.
    function getCountryStateCity(){
    
      $list = array();
        $tempst="";
      $statelist = array();
      $citieslist = array();
      $this->db->select('a.name AS country, b.name AS state ,a.id as country_id ,b.id as state_id, c.name as city, c.id as city_id');
      $this->db->from(TABLE_COUNTRY.' a'); 
      $this->db->join(TABLE_STATE.' b', 'b.country_id=a.id', 'left');
      $this->db->join(TABLE_CITY.' c', 'c.state_id=b.id', 'right');
      $this->db->where('a.id IN (1,58)',NULL,FALSE);
      $this->db->order_by('a.name,b.name,c.name');
      $query = $this->db->get()->result_array();  
      
        
      return $query;
    }

   

    //function to delete record from case assign table
    function assignDelete($id){
      $this->db->where(TABLE_USER_CASE.'.id',$id);
      return $this->db->delete(TABLE_USER_CASE);
      
    }

    //function to get all assigned records for an employee
    function getAssignedDetails($id){
      $list = array();
          $this->db->select('*');
          $this->db->from(TABLE_USER_CASE);
          $this->db->where('empid',$id);
         return $this->db->get()->result_array();
         // echo $this->db->last_query();
    }

    //function to update case assign details
    function updateAssignedDetails($id,$data){
          $this->db->where(TABLE_USER_CASE.'.id',$id);
          $this->db->update(TABLE_USER_CASE, $data);
        
    }

    //function to check any case is assigned to employee 
    function userExist($id){
        $this->db->select('*');
          $this->db->from(TABLE_USER_CASE);
        $query=   $this->db->where('id',$id);
         //$query = $this->db->where_not_in('srno', $id);
         if($query->get()->num_rows() != 0)
         {
           return FALSE;
         }else
         {
           return true;
         }
    }

     public function get_current_page_records($limit=NULL, $start=NULL)  
     {
     
      
       if ($start = -1) {
             $start = 1 ;        # code...
       }
        $this->db->limit($limit, $start);
        $this->db->from("myguests");

                 
         $query =  $this->db->get()->result_array();


//  $query = $this->db->query("SELECT `id`,CONCAT(FIRSTNAME,' ', LASTNAME) AS Name, `email`,`reg_date`,`status`,`stage`,`dateofbirth` FROM myguests LIMIT 5")->result_array();



  
       
      
      return $query;

    }

    public function Sortingnew($limit=NULL, $start=1)  
     {
     
      
       
        
       
        $this->db->order_by($_REQUEST['field'], $_REQUEST['order']);
        $this->db->limit($limit, $start);
        
         //echo $this->db->last_query();
        
         $query =  $this->db->get("myguests")->result_array();


//  $query = $this->db->query("SELECT `id`,CONCAT(FIRSTNAME,' ', LASTNAME) AS Name, `email`,`reg_date`,`status`,`stage`,`dateofbirth` FROM myguests LIMIT 5")->result_array();



  
       
      
      return $query;

    }
     
    public function get_total() 
    {
        return $this->db->count_all("myguests");
         
    }
/*

    public function record_count() 
    {
        return $this->db->count_all("myguests");
    }
  

   // Fetch data according to per_page limit.
     public function fetch_data($limit, $id) 
     {
        $this->db->limit($limit);
        $this->db->where('id', $id);
        $query = $this->db->get("myguests");
        if ($query->num_rows() > 0) {
        foreach ($query->result() as $row) {
        $data[] = $row;
        }

        return $data;
        }
        return false;
        }

  */

        function search($query_string=NULL,$limit=NULL,$offset=NULL,$orderCol='id',$val='asc'){

        //  var_dump($query_string=NULL,$limit=NULL,$offset=NULL,$orderCol='id',$val='asc');
  
      $fields = array('firstname'=> $query_string,
                      'lastname'=> $query_string,
                      'hobbies'=>$query_string,
                      'status'=>$query_string
                     );   

      $this->db->from('myguests');
       if(!empty($query_string))
      {
       
       $this->db->like('firstname', $search_text);
       $this->db->or_like('lastname', $search_text);
       $this->db->or_like('email', $search_text);
       $this->db->or_like('reg_date', $search_text);
       $this->db->or_like('status', $search_text);
       
     };
       $this->db->limit($limi=5,$offset);
    
      $query = $this->db->get();

      $data = $query->result_array();

      return $data;
    }











    // Fetch records
  public function getDatatest($rowno,$rowperpage,$search="",$order,$column) {


      
    $this->db->select('*');
    $this->db->from('myguests');

    if($search != ''){
          $this->db->like('firstname', $search);
      $this->db->or_like('email', $search);
        }

        $this->db->limit($rowperpage, $rowno);

        if ($order!='') 
          $this->db->order_by($column, $order); 


    $query = $this->db->get();

   

        
    return $query->result_array();
  }

 



  // Select total records
    public function getrecordCounttest($search = '') {

      $this->db->select('count(*) as allcount');
        $this->db->from('myguests');
      
        if($search != ''){
          $this->db->like('firstname', $search);
      $this->db->or_like('email', $search);
        }

        $query = $this->db->get();
        $result = $query->result_array();
      
        return $result[0]['allcount'];
    }

    //function to get all uploaded files for zip operation
    function getAllFiles($id)
    {
      $this->db->select('userfile,userimg,userexcel');
      $this->db->from(TABLE_QUERIES);
      $this->db->where('id',$id);
      
      return $this->db->get()->row_array();
    }

// function to retrive db saved profile pic
    function  checkFileExist($id=0,$file){
      if ($file=='img')
      $file = 'userimg';
      if ($file=='pdf')
      $file = 'userfile';
      if ($file=='excel')
      $file = 'userexcel';
      $query = $this->db->query('SELECT '.$file.' FROM `myguests` WHERE `id`='.$id)->row_array();
        return $query[$file];
    }

    //function to update record in database
    function update_record($id,$data)       
    {
      $this->db->where(TABLE_QUERIES.'.id',$id);
        $this->db->update(TABLE_QUERIES, $data);
    }
    

    
}
  

?>












