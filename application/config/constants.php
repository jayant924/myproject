<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('UPLOAD_IMAGE_PATH', 'uploads/');
define('UPLOAD_ZIP_PATH', 'zippedfiles/');
define('UPLOAD_PDF_EXCEL_PATH', 'uploads/');


/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*   table of model  */


define('TABLE_QUERIES','myguests');
define('TABLE_STATE','states');
define('TABLE_COUNTRY','countries');
define('TABLE_CITY','cities');
define('TABLE_USER_CASE','user_case');


define('TOTAL_PAGINATION_NO',		10);  //for toatl no of pagination links
define('BEFORE_AFTER_NO',		5);     //to show pagination links before & after of current page


/* End of file constants.php */
/* Location: ./application/config/constants.php */