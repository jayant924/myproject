<!DOCTYPE html>
<html lang="en">

<head>
    
    
              <title>Assign employee to City</title>
    
    <link href='<?php echo base_url(); ?>css/minified-login.css' rel='stylesheet' type='text/css' />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ui.fancytree.css" >

    <link href="<?php echo site_url('css/bootstrap.min.css')?>" rel="stylesheet">
   
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
   
	
</head>

<body>
  	<?php $this->load->view('include/nav') ;?>

		<div class='container-fluid bg-dark'>
				<div class='row justify-content-center'>
		
			  </div>
		</div>
    <div id="MainContainer">
	  
    </div>

    <script src="<?php echo base_url(); ?>js/minified_js_layout.js"></script>
     <script src="<?php echo base_url(); ?>js/minified-header.js"></script>
    <script src="<?php echo base_url();?>js/jquery.fancytree.js"></script>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.multiselect.js"></script>
   

    <script type="text/javascript">
        jQuery.browser={};(function(){jQuery.browser.msie=false;
		jQuery.browser.version=0;if(navigator.userAgent.match(/MSIE ([0-9]+)\./)){
		jQuery.browser.msie=true;jQuery.browser.version=RegExp.$1;}})();
        //set date format yy-mm-dd
    </script>

    <br/>
	 <center>
		<h2 class='alert-success'>  <?php
        // Show Add/Edit/Delete : Success message
            if(!empty($success_message))
            {
    ?>
                <span id="success_msg" style="color:green;"><?php echo $success_message; ?></span>
    <?php
            }

        // Show Add/Edit : Failed message   
            if(!empty($failed_message))
            {
    ?>
                <span id="fail_msg" style="color:red;"><?php echo $failed_message; ?></span>
    <?php
            }
    ?></h2>
	 </center>
	<br/>
    <div class='container'>
      <div class='row'>
		<div class="col-xs-4">
		<?php $url= ($flag=='edit')? 'welcome/editUpdate':'welcome/saveCase';?>
       

      <form id="cluster_form" name="form2" method="post" action="<?php echo site_url($url);?>"
              enctype="multipart/form-data" autocomplete="off">
          <input name="flag" id="flag" value="save" type="hidden" class="form-group">
		    <div >
				  Select Employee:
				  <br>
				  <br>
				  <?php 
				  echo form_dropdown('empList',$empList,$selected);?>
		    </div>

		  <br/>

			  <div class="clear"></div>

			  <div>
				<input type="button" name="btn_expand" value="Expand All" onclick="expand_all()" class='btn btn-primary btn-sm' /> &nbsp;&nbsp;
				<input type="button" name="btn_collapse" value="Collapse All" onclick="collapse_all()" class='btn btn-primary btn-sm' />
	  
				<br/>

				  <div id="tree" name="selNodes[]" style="margin-top: 10px"></div>
				  <br/>
				  <span class="col-xs-2 col-sm-2 col-md-2">
					  <button type="submit" id="AddCluster" value="Add Cluster" class="btn btn-success btn-sm" >
						<?php echo ($flag=='edit')? 'Update':'Save';?>
					  </button>
					  &nbsp  &nbsp  &nbsp
		
					<?php
							if($flag=='edit'){
							   $caption ="Cancel";
							}else{
							  $caption ="Back";
							}
			
			$back= array(
						    'onclick' => 'back_function()',
						    'id'=>'back'
						 );


		    $back1 = array(
		   	                 'name' =>'back',
		                     'class' => 'btn btn-default btn-sm'
		                  );

							
				//echo form_button('back',, ); // reset button
		               echo anchor('welcome/loadassign', $caption, $back1);				
		          ?>
			</span>
		</form>
		</div>
			</div>
			
		  
		<div id='table' class='col' style="margin-top: -38px">
	<?php if($flag!='edit'){?>

		<nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-top: 35px;">
  <a class="navbar-brand" href="#"><b>Case Information</b></a>
  

  
</nav>
	
	  <table name='caseTable' id='caseTable'  class="table table-hover">
		
		<tr>
                    <thead>

                        <th>Sr.No.</th>
                        <th>Employee Name</th>
						<th>E-Mail</th>
                        <th>Date&nbspAssigned</th>
						<th>Operation</th>
		
                    </thead>
		</tr>
                
         <tbody id="tbody">
           
          <?php 
		     
		         $i = 1;
		        
  

			      if(isset($posts))
			     {
		          foreach(@$posts as $post) {
			      echo "<tr>"; 

			    //echo "<td><center>".$post['srno']."</center></td>";
			      echo "<td><center>".$i."</center></td>";
			      echo "<td>".$post['firstname']."</td>";
	              echo "<td>".$post['email']."</td>";
				  echo "<td>".$post['created_at']."</td>";
			$editbutton = array(
							    'src'   => base_url().'/images/editFav.png',
						        'alt'   => 'Edit',
						        'class' => 'post_images',
						        'width' => '20px',
						        'height'=> '20px'
							   );

			      echo "<td>" .anchor(site_url('/welcome/assignEdit/'.$post['empid']),img($editbutton))."</td>";
			       

			        $onclick = array('onclick'=>"return confirm('Are you sure?')",
			        'src'   => base_url().'/images/deleteFav.png',
			        'alt'   => 'Delete',
			        'class' => 'post_images',
			        'width' => '21px',
			        'height'=> '20px'
			      );
			      echo "<td>" . anchor(site_url('/welcome/assignDelete/'.$post['id']),img($onclick))."</td>";
				   echo "</tr>";
				 $i++;
			 }
			 }
			  ?>
				</tbody>
	  </table>
	  </div>
	</div>
		  </div>
<?php } ?>



    <script type='text/javascript'>


        //alert('<?php echo @$var_json; ?>');
	<?php
	
		  if (!empty($countrylist))
	  {
	?>
		$(function() {
            $("#tree").fancytree({
                checkbox: true,
                selectMode: 3,
                icons: false,
                source: <?php echo @$countrylist; ?>,

            });
            $("#cluster_form").submit(function() {
                // Render hidden <input> elements for active and selected nodes
                $("#tree").fancytree("getTree").generateFormElements();
              //  alert(jQuery.param($(this).serializeArray()));
               //return false;

            });
        });

        function expand_all() {
            $("#tree").fancytree("getRootNode").visit(function(node) {
                node.setExpanded(true);
            });
        }

        function collapse_all() {
            $("#tree").fancytree("getRootNode").visit(function(node) {
                node.setExpanded(false);
            });
        }
<?php } ?>

function back_function() {
			flag = "<?php echo $flag;?>";
			if (flag=='edit') {
			 window.location.href = "<?php  echo site_url('welcome/loadAssign/0'); ?>";
			}else{
			  window.location.href = "<?php  echo site_url('welcome/index/0'); ?>";
			}
            
        }
    </script>

    <script>
  $(function() {

            $('#likes').multiselect({
                columns: 1,
                placeholder: 'select Subjects',
                search: true,
                selectAll: true
            });
        });


</script>

</body>
</html>