<?php
  print_r($posts); die();
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Shimbi Technology lab</title>


   <link rel="stylesheet" href="<?php echo base_url();?>css/minmize_date_picker.css" />  
    <link href="<?php echo base_url(); ?>css/jquery.multiselect.css" rel="stylesheet">
    <!--bootstrap-->
    <link href="<?php echo base_url(); ?>css/home/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/fonts/font-awesome.min.css" rel="stylesheet">
    
    <link href="<?php echo base_url(); ?>css/jquery.fancybox.css" rel="stylesheet">

   
</head>
<body>
	   <?php $this->load->view('include/nav');?>

	   <ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo base_url();?>welcome/home">Home</a></li>
  <li class="breadcrumb-item active">All Records</li>
</ol>

<div class="container">
  <div class="col-md-12">


<div class="row" style="padding-top: 50px;">
    <div class="col">

   

	<?php echo anchor('welcome/export_to_excel_new', 'export to excel',['class'=>'btn btn-success']);?>

</div>
<div class="col" style="padding-left: 243px;">


    <form action="<?php echo base_url();?>auth/profile" class="form-inline my-2 my-lg-0" 
         method='post' >

        <?php
                    // Form input : text
                    $data=array(
                                'name'=>'search',
                                'type'=>'text',
                                'id'=>'removeexcel',
                                'class'=>"form-control",
                                'placeholder'=>"Search Here",
                                'value'=> $search
                            );
                    
                   echo form_input($data); 
     ?>
     
         
       

                              <?php
                                  $data_bt = array(
                                                     'name'  => 'submit',
                                                     'id'  => 'submit_btn',
                                                     'class' => 'btn btn-default',
                                                     'style' => 'margin-left: 87px'
                                                     
                                                
                                                   );

                                  
                                  
                                  echo form_submit($data_bt,$this->lang->line("SEARCH"));
                             ?>




    </form> 
</div>

<div style="padding-left: 10px;">
 <form class="form-inline" method='post' >

           <?php echo anchor('welcome/add', 'Add New',['class'=>'btn btn-primary']);?>
          </form>

</div>







</div>


         

  </div>
	<br/>

	<!-- Posts List -->
	<table class="table table-stripd table-hover">
		<tr class="table-primary">
			<th>S.no</th>
			             <th>Name 
                    
    <a href="<?php echo base_url().'auth/sorting?field=firstname&order=ASC'?>"> <i class="fa fa-long-arrow-up" style="color: #8f8f8f;"></i></a>
    <a href="<?php echo base_url().'auth/sorting?field=firstname&order=DESC'?>"> <i class="fa fa-long-arrow-down"
      style="color: #8f8f8f;"></i></a>

              </th>
              

              <th>email
    <a href="<?php echo base_url().'auth/sorting?field=email&order=ASC'?>"> <i class="fa fa-long-arrow-up" style="color: #8f8f8f;"></i></a>
    <a href="<?php echo base_url().'auth/sorting?field=email&order=DESC'?>"> <i class="fa fa-long-arrow-down" style="color: #8f8f8f;"></i></a>
   

             </th>
              
              <th>Register Date 
  <a href="<?php echo base_url().'auth/sorting?field=reg_date&order=ASC'?>"> <i class="fa fa-long-arrow-up" style="color: #8f8f8f;"></i>
  </a>
  <a href="<?php echo base_url().'auth/sorting?field=reg_date&order=DESC'?>"> <i class="fa fa-long-arrow-down" style="color: #8f8f8f;"></i></a>


             </th>
              
              <th>Gender</th>
              <th>Status</th>
              <th>Cp</th>
              <th>View</th>
              <th>Action</th>

		</tr>
		<?php 
		$sno = $row+1;
		foreach($result as $posts){

			  $act_image = array(
                                         'src'   => base_url().'images/act.jpg'.'?'.time(),
                                         'alt'   => 'Activated',
                                         'id'   => 'actImage'.$posts['id'],
                                         'onclick'=> 'javascript:toggleStatus('.$posts['id'].',1);',
                                         'class' => 'post_images',
                                         'width' => '20px',
                                         'height'=> '20px',
                                         'style'=>'border-radius:50%'

                                       );

      $deact_image = array(
                             'src'   => base_url().'images/deact.jpg'.'?'.time(),
                             'alt'   => 'deactivated ',
                             'class' => 'post_images',
                             'id'   => 'DeactImage'.$posts['id'],
                             'onclick'=> 'javascript:toggleStatus('.$posts['id'].',0);',
                             'width' => '20px',
                             'height'=> '20px',
                             'style'=>'border-radius:50%'

                           );
        $cp_image = array(
                             'src'   => base_url().'images/cp.png',
                             'alt'   => 'change password ',
                             
                             'id'   => 'cp'.$posts['id'],
                             'onclick'=> 'javascript:changePass('.$posts['id'].');',
                             'width' => '15px',
                             'height'=> '15px'  
   

                         );
         $confirm = 'onclick="return confirm(\'Are you sure you want to delete this data ?\')"'; 

			
			echo "<tr>";
			echo "<td>".$sno."</td>";
			echo "<td>".$posts['firstname']." ".$posts['lastname']."</a></td>";
			echo "<td>".$posts['email']."</td>";
			echo "<td>".date("d-m-Y",strtotime($posts['reg_date']))."</td>";
			echo "<td>".$posts['status']."</td>";

              if($posts['stage']=='1')

             {
                   echo "<td align='center'>".img($act_image)."</td>";
             }
               else
             {
                   echo "<td align='center'>".img($deact_image)."</td>";
             }

            echo "<td>"  .img($cp_image)."</td>";



            echo "<td>".anchor('/welcome/generatepdf/' . $posts['id'], '<i style="font-size:18px" class="fa">&#xf1c1;</i>')."</td>";
                        
                     
        



          echo "<td>".anchor('/welcome/edit/' . $posts['id'], '<i class="fa fa-edit"></i>')
                           .' / '.
                      anchor('welcome/generateZipnew/' . $posts['id'], '<i class="fa fa-file-zip-o"></i>')
                            .' / '.
          
         anchor('/welcome/delete/' . $posts['id'], '<i style="font-size:24px" class="fa">&#xf00d;</i>', $confirm).
                           
                            
                  "</td>";

                  
                         
   
			



			echo "</tr>";

			
			$sno++;

		}
		if(count($result) == 0){
			echo "<tr>";
			echo "<td colspan='3'>No record found.</td>";
			echo "</tr>";
		}
		?>
	</table>

	<!-- Paginate -->
	<div>
  <ul class="pagination pagination-lg">
    <li class="page-item disabled">
      <?= $pagination; ?>
    </li>
	
	</div>

</div>


<div>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Total Records : <?php echo $total_rows; ?></a>
   <p> 
    <span style="padding-left: 750px;"> 
      <center>

    Copyright &copy Shim-Bi Labs, <?php echo date('Y'); ?>

  </center>
</span>
</p>

</nav>
    
  </div>
</div>
</div>
</div>

</body>
 <script src="<?php echo base_url();?>js/minmize_date_picker.js"></script>
  <script src="<?php echo base_url();?>js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.multiselect.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
 
  <script src="<?php echo base_url(); ?>js/jquery.fancybox.js"></script>

<script type="text/javascript">
   $(function() {
            $("#startdate").datepicker({
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy', // Date format
                yearRange: '1940:<?php echo date("Y");?>'
            }).attr('readonly', 'readonly');
        });
</script>

<script type="text/javascript">
   $(function() {
            $("#enddate").datepicker({
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy', // Date format
                yearRange: '1940:<?php echo date("Y");?>'
            }).attr('readonly', 'readonly');
        });
</script>







  <script type="text/javascript">
        $(function() {
            $("#dob_idto").datepicker({
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy', // Date format
                yearRange: '1940:<?php echo date("Y");?>'
            }).attr('readonly', 'readonly');
        });

        function cleardate() {
            // $("#dob_id").val =="";//._clearDate(this);
            document.getElementById("dob_idto").value = "";
        }
    </script>

    <script>

    (function() {
    $('#submit').on('click', function(){
      var date = new Date($('#date-input').val());
      day = date.getDate();
      month = date.getMonth() + 1;
      year = date.getFullYear();
      alert([day, month, year].join('/'));
    });
})();
</script>

    <script type="text/javascript">
        $(function() {
            $("#date-input").datepicker({
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy', // Date format
                yearRange: '1940:<?php echo date("Y");?>'
            }).attr('readonly', 'readonly');
        });

        function cleardate() {
            // $("#dob_id").val =="";//._clearDate(this);
            document.getElementById("dob_idfrom").value = "";
        }
    </script>

    <!--function to toggle user status -->

      <script type="text/javascript">
$("a#iframe").fancybox({
       'type': 'image',
       openEffect: 'elastic',
       closeEffect: 'elastic',

   });

   $(function() {
// function to use jquery datepicker 
       $("#datepickfrom").datepicker({
           showButtonPanel: true,
           changeMonth: true,
           changeYear: true,
           dateFormat: 'dd-mm-yy', // Date format
           yearRange: '1940:<?php echo date("Y");?>'
       }).attr('readonly', 'readonly');

       $("#datepickto").datepicker({
           showButtonPanel: true,
           changeMonth: true,
           changeYear: true,
           dateFormat: 'dd-mm-yy', // Date format
           yearRange: '1940:<?php echo date("Y");?>'
       }).attr('readonly', 'readonly');
   });
//function for clearing date field
   function cleardatet(id) {
       // $("#dob_id").val =="";//._clearDate(this);
       document.getElementById(id).value = "";
   }





//function to toggle user status
   function toggleStatus(id, status) {

       var msg;
       $.ajax({
           type: "POST",
           dataType: "json",

           url: "<?php echo site_url('/welcome/changeStatus'); ?>",
           data: {
               id: id,
               status: status
           },
           success: function(data) {
               // jQuery.noConflict();

               if (status != 0) {

                   $('#actImage' + id).attr({
                       'src': '<?php echo base_url().'images/Deact.jpg?'.time();?>',
                       'id' : 'DeactImage' + id,
                       'onclick': 'javascript:toggleStatus(' + id + ',0)'
                   });
                   msg = 'deactivated';

               } else {

                   $('#DeactImage' + id).attr({
                       'src': '<?php echo base_url().'images/act.jpg ? '.time();?>',
                       'id' : 'actImage' + id,
                       'onclick': 'javascript:toggleStatus(' + id + ',1)'
                   });
                   msg = 'activated';

               }

               var abc = '<h3 style="color: black;">User ' + data[0]['firstname'] +' ' + msg + ' successfully!</h3>';
               $.fancybox.open({

                   content: abc, //'<div id="test">Status updated successfully for user '+</div>',
                   'closeBtn': true,
                   'padding': 10,
                   'scrolling': 'no',
                   openEffect: 'fade',
                   closeEffect: 'fade',
                   'type': 'iframe',
                   // href : "<?php echo base_url().'success';?>",

               });

           },
           error: function(data) {
               var abc = 'error function';
               console.log('this is ' + abc);
           }
       });
   }


// ..............................function to change password in fancybox


   function changePass(id) {
       //alert(id);
       // jQuery.noConflict();
       $.fancybox.open({

           //content  : '<div id="test">Status updated successfully for user '+'</div>',
           'closeBtn': true,
           'padding': 10,
           'type': 'iframe',
           href: "<?php echo site_url('welcome/changePassword');?>" +'/'+ id,
           openEffect: 'elastic',
           closeEffect: 'elastic',
           'width': 600,
           // modal:true,
           keys: {
               close: null
           },

           'height': 420,
           'autoSize': false,
           helpers: {
               overlay: {
                   closeClick: false
               } // prevents closing when clicking OUTSIDE fancybox 
           },
           afterShow: function() {
               this.inner.css({
                   overflow: 'hidden',

               })
           },

       });
   }


     </script>
   





</html>
</html>