
<!DOCTYPE html>
<html>
<head>
  <!--DatePicker-->
    <link rel="stylesheet" href="<?php echo base_url();?>css/minmize_date_picker.css" />
    <!--Multiselect-->
    <link href="<?php echo base_url(); ?>css/jquery-ui.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery.multiselect.css" rel="stylesheet">
    <!--bootstrap-->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/datatables.min.css" rel="stylesheet">
   
</head>
<body>


<?php if ($flag =='add')
    {
      $action= site_url('/addstd');
      $reset ='Reset';
      $country='';
      $state ='';
      $city ='';
      $city_selected ='';
      $state_selected="";
      $citieslist="";
      $statelist="";
      $country_selected="";
      $password = "";
      $gender = "";
      $passconf="";

    }
    if($flag == 'upd')
    {
      $action = site_url('/updatestd/'.$id);
      $reset = 'Cancel';

    }?>
<?php



$setexcel = (isset($post['userexcel'])|| $post['userexcel']!='')?'display:none':'';
$hideExlDel = (!isset($post['userexcel']) || $post['userexcel']=='')?'display:none':'';
$showFileExcel = (!isset($post['userexcel']) || $post['userexcel']  =='')?'':'display:none';
$setimg = (isset($post['userimg']) || $post['userimg']!='')?'display:none':'';  //if image available hide

$showFileImg = (!isset($post['userimg'])|| $post['userimg']=='')?'':'display:none';
$displayImage = (!isset($post['userimg'])|| $post['userimg']=='')?'display:none':'';  
$setpdf = (isset($post['userfile'])|| $post['userfile']!='')?'display:none':'';       // if pdf available hide
$hidePdfDel = (!isset($post['userfile'])|| $post['userfile']=='')?'display:none':'';          // if pdf not available hide
$showFilePdf = (!isset($post['userfile'])|| $post['userfile']=='')?'':'display:none';         // if pdf not available hide  

    $delImgImg = array(
                        'src'   => base_url().'/images/del.jpg',
                        'id'=>'removeimage',
                        'alt'   => 'Remove Image',
                        'class' => 'post_images',
                     
                        'width' => '50',
                        'height'=> '42',
                        'style'=>'width: 125px;'.$post['userimg']
                      
                );


      $excel_img = array(
                          'src'   => base_url().'/images/excel.png',
                          'alt'   => 'Excel Download',
                          'class' => 'post_images',
                          'width' => '150',
                          'height'=> '100',
                          'id'=>'exceluploadImg',
                          'style'=> 'margin-top:-18px;'.$hideExlDel
      
                        );

          $delImgPdf = array(
                              'src'   => base_url().'/images/del.jpg',
                              'id'=>'removepdf',
                              'alt'   => 'Delete PDF File',
                              'class' => 'post_images',
                              'style'=> 'width: 125px;',
                              'width' => '50',
                              'height'=> '42',
                              'style'=> $hidePdfDel
                          

                            
                            );

    $image_properties = array(
                              'src'   => base_url().'/images/pdf.png',
                              'alt'   => 'PDF Download',
                              'class' => 'post_images',
                              'width' => '100',
                              'height'=> '100',
                              'id'=>'pdfuploadImg',
                              'style'=> 'margin-top:-18px;'.$hidePdfDel
                  
                               );

    
  

      $delImgExl = array(
                          'src'   => base_url().'/images/del.jpg',
                          'id'=>'removeexcel',
                          'alt'   => 'Delete Excel File',
                          'class' => 'post_images',
                          'style'=> 'margin-top:17px',
                          'width' => '50',
                          'height'=> '50',
                          'style'=> $hideExlDel
                        );
    

   

?>



<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo base_url();?>auth/profile">Home</a></li>
  <li class="breadcrumb-item active">Insert New</li>
</ol>
<br>
<br>

<div class="container">
  <div class="col-md-12">
  <div class="row">


<div class="main_tag">
    <?php
        
        // Form open
        echo form_open_multipart(@$form_action, array('method' => 'post'));
        echo form_hidden('flag',@$flag);

         $hiddenInput = array(
                            'flagExcel'  => 1,
                            'flagPdf'  => 1,
                            'flagImg'  => 1
                        );  

    echo form_hidden($hiddenInput);
    ?>
</div>

 
   <div class="col">
     <label for="Firstname"><b>Firstname</b></label>
    
      
  <?php
                    // Form input : text
                    $data=array(
                                'name'=>'firstname',
                                'id'=>'first_name',
                                'class'=>'form-control',
                                'value'=>@$post['firstname']
                            );
                    
                    echo form_input($data);
     ?>
                                           

   



   </div>
   <div class="col  text-danger">
                      <?php echo form_error('firstname'); ?>
   </div>

    <br>

    <div class="col">
     <label for="lastname"><b>Lastname</b></label>
    
 <?php
                    // Form input : text
                    $data=array(
                                'name'=>'lastname',
                                'id'=>'last_name',
                                 'class'=>'form-control',
                                'value'=>@$post['lastname']
                            );
                    
                    echo form_input($data);
     ?>
                                              <br/>

  



     


   </div>

     <div class="col  text-danger">
                    <?php echo form_error('lastname'); ?>
     </div>
   </div>
    <br>

  <div class="row">
    <div class="col col form-group has-success">
      <label for="exampleInputEmail1"><b>Email address</b></label>

        <?php
                    // Form input : text
                    $data=array(
                                'name'=>'email',
                                'id'=>'e_mail',
                                 'class'=>'form-control is-valid',
                                'value'=>@$post['email']
                            );
                    
                    echo form_input($data);
     ?>
     <div class="valid-feedback">Note down this Unique E-mail for Log In</div>
                                              <br/>

   

     
    </div>
      <div class="col text-danger">
           <?php echo form_error('email'); ?>
     </div>
     
    <br>
    <br>



   <div class="col form-group has-success">
     <label for="password"><b>Password</b></label>

    
      <?php
                    // Form input : text
                    $data=array(
                                'name'=>'password',
                                'type'=>'password',
                                'id'=>'pass_word',
                                'class'=>"form-control is-valid",
                                'value'=>@$post['password']
                            );
                    
                   echo form_input($data); 
     ?>
  <div class="valid-feedback">Note down this password for Log In</div>
                                              <br/>

   



   </div>
   <div class="col  text-danger">
                      <?php echo form_error('password'); ?>
   </div>
  </div>
    <br>




      <br>
  <br>
<br>

    <div class="row">
   
         <div class="col">

                     <div class="form-group">
                       <label for="exampleSelect1"><b>Country</b></label>
            <select class="form-control" id="countryname" name="country" value="<?php echo $post['country']; ?>" onchange="getState();">
                                <option>Please Select</option>
       <?php
             foreach($countries as $countryd)
             {
               if ($post['country']!="" && @$post['country']==$countryd['id'])
               {
                   echo "<option value='".$countryd['id']."'  selected='true'>".$countryd['name']."</option>";
               }
               else
               {
                echo "<option value='".$countryd['id']."'>".$countryd['name']."</option>";
              }
             }

       ?>
                         </select>  
                      </div>

         </div>

          <div class="col  text-danger">

                      <?php echo form_error('country'); ?>
   </div>
       
    
         <div class="col">

                    <div class="form-group">
                       <label for="exampleSelect1"><b>State</b></label>
                <select class="form-control" id="statename" name="state"  onchange="getcity();">
                                <option>Please Select</option>
                              
                         </select>
                      </div>      

         </div>

         
          <div class="col  text-danger">
                      <?php echo form_error('state'); ?>
         </div>

     </div>

      <br>
<br>

    <div class="row">
   
         <div class="col">

              
                    <div class="form-group">
                       <label for="exampleSelect1"><b>City</b></label>
                          <select class="form-control" id="cityname" name="city" >
                                <option>Please Select</option>
                             
                         </select>
                      </div>  

         </div>

                     <div class="col  text-danger">
                               <?php echo form_error('city'); ?>
                     </div>

       <div class="col">

          <div class="form-group">
      
                 <label for="exampleTextarea">Comment :</label>
            


        <?php
                    // Form input : text
                    $data=array(
                                'name'=>'comment',
                                'type'=>'textarea',
                                'rows'=>'1',
                                'class'=>"form-control",
                                'value'=>@$post['comment']
                            );
                    
                   echo form_input($data); 
     ?>
                         




                      </div>  

         </div>

                     <div class="col  text-danger">
                               <?php echo form_error('City'); ?>
                     </div>


       </div>

       <br>
       <br>
       <br>

    <div class="row">
      <div class="col">
       

           
              <div class="form-group">
   
   
        <label class="form-control"><b style="color: red;">Choose PDF :</b></label>
     
   <!-- download pdf file -->
        <?php echo anchor(base_url() . UPLOAD_PDF_EXCEL_PATH . $post['userfile'] ,img($image_properties)); 

// Remove pdf file -->      
       // echo img($delImgPdf); 
      ?>
      <br>
     <a  class="alert-link">Click to View PDF</a>
    


              <?php
                    // Form input : text
                    $data=array(
                                'name'=>'userfile',
                                'type'=>'file',
                                'style'=>$showFilePdf,
                                'id'=>'userpdf',
                                'class'=>"form-control-file",
                                'value'=>@$post['userfile']
                            );
                    
                   echo form_input($data); 
                  
     ?>

    
     
</div>




</div>

                     <div class="col  text-danger">

                         <button type="button" id="removepdf" class="btn btn-danger btn-sm">Remove</button>
                               <?php echo form_error('userfile'); ?>
                     </div>



<div class="col">

<label for="lastname"><b>Date Of Birth</b></label>


    
 <?php
                    // Form input : text
                    $data=array(
                                'name'=>'dateofbirth',
                                'id'=>'dob_id',
                                 'class'=>'form-control',
                                 'type' =>'text',
                                'value'=>date("d-m-Y",strtotime($post['dateofbirth']))
                            );
                    
                    echo form_input($data);

     ?>
        


  </div>



    <div class="col  text-danger">
       <button type="button" onclick="cleardate()" class="btn btn-default btn-sm">Reset</button>
                               <?php echo form_error('dateofbirth'); ?>
                     </div>


           </div>
         

                <br>
       <br>
       <br>

    <div class="row">
      <div class="col">
       

           
              <div class="form-group">
   
   
        <label class="form-control"><b style="color: blue;">Choose IMG :</b></label>

  <?php 

      if(isset($post['userimg']) && $post['userimg']!='')

   ?>

      <img  style="<?php echo $displayImage?>" id='usrImage' 
      src="<?php echo  base_url() . UPLOAD_IMAGE_PATH .$post['userimg'].'?'.time();  ?>" 
      width="200px" height="200px" alt=' No-Image'>
                    

    
      <br>
     <a  class="alert-link">It's your Profile Picture</a>


          

  
              <?php
                    // Form input : text
                    $data=array(
                                'name'=>'userimg',
                                'type'=>'file',
                                'id'=>'userimg',
                                'class'=>"form-control-file",
                                'value'=>@$post['userimg']
                            );
                    
                   echo form_input($data); 
     ?>

    
     
</div>




</div>

                     <div class="col  text-danger">

                      <button type="button" id="removeimage"  class="btn btn-primary btn-sm">Remove</button>
                    
                               <?php echo form_error('userimg'); ?>
                     </div>



<div class="col">

<label class="form-control"><b style="color: green;">Choose EXCEL :</b></label>

         
       <?php 
        echo anchor(base_url() .UPLOAD_PDF_EXCEL_PATH . $post['userexcel'],img($excel_img)); //download excel file
       // echo img($delImgExl); // remove excel file
     ?>
     <br>
     <a  class="alert-link">Click to View Excel</a>


              <?php
                    // Form input : text
                    $data=array(
                                'name'=>'userexcel',
                                'type'=>'file',
                                'style'=>$showFileExcel,
                                'id'=>'userexcel',
                                'class'=>"form-control-file",
                                'value'=>@$post['userexcel']
                            );
                    
                   echo form_input($data); 
     ?>
        


  </div>



    <div class="col  text-danger">
                     <button type="button" id="removeexcel" class="btn btn-success btn-sm">Remove</button>
                               <?php echo form_error('userexcel'); ?>
                     </div>


           </div>

 





<br>
<br>



<br>
<br>
<br>
<br>

<div class="row">
<div class="col">
    <legend>Gender</legend>
      




 <?php

                    //Form input : radio buttons
                    $data=array(
                                'name'=>'status',
                                'id'=>'status_1',
                                'value'=>'male',
                                

                                'checked'=>(@$post['status']=='male')?'checked':false,
                            );
                    echo form_radio($data);
                    
                      echo " Male";


                    
                    echo "&nbsp;&nbsp;&nbsp;";
                    
                    $data=array(
                                'name'=>'status',
                                'id'=>'status_2',
                                'value'=>'female',
                                'checked'=>(@$post['status']=='female')?'checked':false,
                            );
                    echo form_radio($data);
                    
                    echo " Female";
                        
                ?>
                <br>
                <br>
                    
 
    </div>

    <div class="col text-danger">
    <?php echo form_error('status'); ?>
     </div>

   
          <div class="col form-control">
                         <legend>Hobbies</legend>
  
                        

                  <?php  

                     $ono= array(
                                       '1'=> 'Fishing',
                                       '2'=> 'Playing',
                                       '3'=> 'Swimming',
                                       '4'=> 'Gardening'
                                 );

                $temp ='class="custom-select col-md-1" multiple="multiple" id="likes"';
                                
                             
                    
                   
                    echo form_multiselect('hobbies[]',$ono,explode(",", $post['hobbies']),$temp);                 

                  ?>
                    
<br>

                    
                    

   </div> 
         
          <div class="col text-danger">
                 
                  <?php echo form_error('hobbies[]'); ?>
          
          </div>

 
  
    
    </div>

 
 <br>
    <br>
     
<div class="row">
   <nav class="navbar navbar-expand-lg navbar-light bg-light">

<div class="col" style="height: 600px; width: 800px;">
    <legend>About Me:</legend>
      



 <?php $data = array(
                                   'name'        => 'aboutme',
                                   'id'          => 'layout',
                                   'value'       => @$post['aboutme'],
                                   
                               );
           echo form_textarea($data);?></td>
   

    
     </div>


<div class="col" style="
    padding-top: 0px;
    padding-left: 17px;
    height: 346px;
">

<div class="card mb-3">
  <h3 class="card-header">SHIMBI LABS.</h3>

<?php 
   // echo realpath('./uploads/'.@$post['userfile']);
      if(is_file(realpath('./uploads/'.@$post['userimg'])))
      {
?>

  <img style="height: 200px; width: 100%; display: block;" id='userImage' src="<?php echo base_url(); ?>uploads/<?php echo @$post['userimg']; ?>"><br>
   
<?php } ?>

  <div class="card-footer text-muted">
    <?php echo @$post['firstname']."&nbsp".@$post['lastname']. "<br>".@$post['reg_date'];?>
  
    <br>
    <br>
    
    Shimbi Technology Lab
  </div>
</div>

</nav>
  </div>
</div>

<br>
<br>
<br>
<br>

<div class="row">
<div class="col">

<div class="alert alert-dismissible alert-secondary">
  <img src="<?php echo base_url();?>images/excel.png" style="height: 100px; width: 200px;">

                                 
   <br>
   <a href="#" class="alert-link">Change a few things up</a> and try submitting again.
</div>

  </div>
  <div class="col">

<div class="alert alert-dismissible alert-secondary">
  <img src="<?php echo base_url();?>images/pdf.png" style="height: 100px; width: 120px;">

         
  <br> 
  <a href="#" class="alert-link">Change a few things up</a> and try submitting again.
</div>

  </div>
  
</div>


 



    <br>
    <br>

    <div class="row">

 <?php
              $data_bt = array(
                  'name'  => 'submit',
                  'id'  => 'submit_btn',
                  'class' => 'btn btn-primary',

                                    'style'=>'cursor:pointer;',
                                );
              echo form_submit($data_bt,$this->lang->line("SAVE"));
          ?>
                    &nbsp;&nbsp;
                    <?php
                        $data_bt = array(
                                
                                'name'  => 'mybutton',
                                'id'  => 'back_btn',
                                'type'  => 'button',
                                'class' => 'btn btn-default',
                                'style'=>'cursor:pointer;',
                                'onclick' => "back_function();"
                            );
                       
                        echo form_button($data_bt,$this->lang->line("BACK"));
          ?>
                </span>
            </td>
        </tr>
    </table>
<?php echo form_close(); // form close ?>
</div>
   </div>






<script type="text/javascript">
    function back_function()
    {
        window.location.href="<?php echo base_url('/auth/profile'); ?>";
    }
</script>

 

</div>
</div>
</div>
</div>


<div style="padding-top: 100px;">
<nav class="n navbar-primary bg-primary">
  <a class="navbar-brand" style="color: white;" href="#"><marquee>Welcome in Shimbi Technology Lab</marquee></a>
  

  
  </div>

<!-- For Footer -->













</body>
 <script src="<?php echo base_url();?>js/minmize_date_picker.js"></script>
<script src="<?php echo base_url();?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.multiselect.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/datatables.min.js"></script>

      
<!-- For Multiple Dropdown Checkbox-->

<script>
  $(function() {

            $('#likes').multiselect({
                columns: 1,
                placeholder: 'select hobbies',
                search: true,
                selectAll: true
            });
        });


</script>

<!--For jquery DatePicker -->


 <script type="text/javascript">
        $(function() {
            $("#dob_id").datepicker({
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy', // Date format
                yearRange: '1940:<?php echo date("Y");?>'
            }).attr('readonly', 'readonly');
        });

        function cleardate() {
            // $("#dob_id").val =="";//._clearDate(this);
            document.getElementById("dob_id").value = "";
        }
    </script>


<!-- For Ck Editor -->


       <script type="text/javascript">
                   CKEDITOR.replace('layout',
                   {
                           toolbar : 'MyToolbar',
                           width:'792',
                           height:'332',
                           filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
                           filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?type=Images',
                           filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?type=Flash',
                           filebrowserUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                           filebrowserImageUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                           filebrowserFlashUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                   });
           </script>

    <!-- for country,city state drop-down  -->
  
 

 <script>
           
        function getState()
  {
    var country_id = $('#countryname').val();
    
    if (country_id!='' && country_id!=undefined && country_id!=0)
    {
      $.ajax({
      
        type: 'POST',
        url: "<?php echo base_url();?>welcome/get_country_state",
        data: { 'country_id':country_id},
        dataType:'json',
        //async: false, //blocks window close (one at a time request) (causes inspect element warning in chrome)
        success: function(data)
        {
          
          if (data!='no_data')
          {
            $('#statename').empty();
            
            $('#statename').append($("<option></option>").attr("value",'').text('Select State'));
            
            $.each(data, function(id,val)
            { 
              if (id=='<?php echo @$post['state']; ?>')
              {
                $('#statename').append($("<option selected></option>").attr("value", id).text(val));
              }
              else
              {
                
                $('#statename').append($("<option></option>").attr("value", id).text(val));
                
              }     
            
            });
            
              getcity();   
    
            
            
          }
            
        }
      });
    }
    
  }

  // for city selection ajax


   function getcity()
  {
    var state_id = $('#statename').val();
    
    if (state_id!='' && state_id!=undefined && state_id!=0)
    {
      $.ajax({
      
        type: 'POST',
        url: "<?php echo base_url();?>welcome/get_state_city",
        data: { 'state_id':state_id},
        dataType:'json',
        //async: false, //blocks window close (one at a time request) (causes inspect element warning in chrome)
        success: function(data)
        {
          
          if (data!='no_data')
          {
            $('#cityname').empty();
            
            $('#cityname').append($("<option></option>").attr("value",'').text('Select City'));
            
            $.each(data, function(id,val)
            { 
              if (id=='<?php echo @$post['city']; ?>')
              {
                $('#cityname').append($("<option selected></option>").attr("value", id).text(val));
              }
              else
              {
                
                $('#cityname').append($("<option></option>").attr("value", id).text(val));
                
              }     
            
            });
            
            
            
          }
            
        }
      });
    }
    
  }
  $(window).load(function() 
  {
     getState(); 
   
  });
  
  

        </script>

        <script type="text/javascript">

            
    
        $(document).ready(function () {         
            $('#removeimage').click(function(){
            $(this).hide();
            $('#usrImage').hide();
            $('input[name=flagImg]').val(0);
            $('#userimg').show();
                     
           });


           $('#removepdf').click(function(){
           $(this).hide();
           $('#pdfuploadImg').hide();
           $('input[name=flagPdf]').val(0);
           $('#userpdf').show();
                     
           });
           
             $('#removeexcel').click(function(){
             $(this).hide();
             $('#exceluploadImg').hide();
             $('input[name=flagExcel]').val(0);
             $('#userexcel').show();
  
            });
         
           });
             
           </script>
</html>