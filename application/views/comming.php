<!DOCTYPE html>
<html>
<head>
  <!--DatePicker-->
    <link rel="stylesheet" href="<?php echo base_url();?>css/minmize_date_picker.css" />
    <!--Multiselect-->
    <link href="<?php echo base_url(); ?>css/jquery-ui.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery.multiselect.css" rel="stylesheet">
    <!--bootstrap-->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
</head>
<body>



<?php $this->load->view('include/nav');?>



<?php $this->load->view($viewPage);?>



</body>
 <script src="<?php echo base_url();?>js/minmize_date_picker.js"></script>
<script src="<?php echo base_url();?>js/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.multiselect.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

      


<script>
  $(function() {

            $('#likes').multiselect({
                columns: 1,
                placeholder: 'select Subjects',
                search: true,
                selectAll: true
            });
        });


</script>


 <script type="text/javascript">
        $(function() {
            $("#dob_id").datepicker({
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy', // Date format
                yearRange: '1940:<?php echo date("Y");?>'
            }).attr('readonly', 'readonly');
        });

        function cleardatet() {
            // $("#dob_id").val =="";//._clearDate(this);
            document.getElementById("dob_id").value = "";
        }
    </script>

</html>