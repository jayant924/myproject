<html>
  <head>
    
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/home/bootstrap.min.css">
  <body>
  
    



      <form  class='form' method='post' action='<?php echo site_url('/welcome/change/'.$id)?>'>
      <div class="container bootstrap snippet">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="breadcrumb" style="margin-top: 10px;">
                        <span style="color: black;"></span>
                        Change password   
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 separator social-login-box"> <br>
                           <img alt="No Image" class="img-thumbnail" src="<?php echo base_url();?>images/jay.jpg">                        
                        </div>
                        <div style="margin-top:80px;" class="col-xs-6 col-sm-6 col-md-6 login-box">
                         <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                              <input class="form-control" type="password" placeholder="New Password" name='newpass'>
                                 
                            </div>
                            <?php echo form_error('newpass')?>
                          </div>
                          <div class="form-group">
                            <div class="input-group">
                              <div class="input-group-addon"><span class="glyphicon glyphicon-log-in"></span></div>
                              <input class="form-control" type="password" placeholder="Re-enter Password" name='checkpass'>
                                
                            </div>
                             <?php echo form_error('checkpass')?>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6"></div>
                        <div class="col-xs-2 col-sm-2 col-md-2">
                            <button class="btn icon-btn-save btn-success col-xs-4" type="submit">
                            <span class="btn-save-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>Save</button>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3">
                            <button type="button" class="btn btn-grey" id='btnclose'>
                            <span class="btn-save-label"><i class="glyphicon glyphicon-floppy-disk"></i></span>Close</button>
                        </div>
                    

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    </form>

     <center>
<span>
  <br/>
    Copyright &copy Shim-Bi Labs, <?php echo date('Y'); ?>
  </center>   


</html>

  <script src="<?php echo base_url();?>js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
 <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
   
   
          <script type='text/javascript'>
      $("#btnclose").click(function() {
       
       parent.jQuery.fancybox.close();
       
      });
    </script> 
 