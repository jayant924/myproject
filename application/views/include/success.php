<html>
  <head>
<link href="<?php echo base_url(); ?>css/jquery-ui.css" rel="stylesheet">
    <!--bootstrap-->
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    
    <div class="jumbotron text-xs-center">
  <h1 class="display-3">Thank You!</h1>
  <p class="lead"><strong>Password changed successfully!</p>
  <hr>
  
 <button type="button" class="btn btn-grey" id='btnclose'>Close</button>
</div>
    <div id="footer_bottom" class="footer-bottom">
  
  <script src="<?php echo base_url();?>js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
 <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<div class="container">
<div class="row">
<div class="col">
  <center>
<span>
  <br/>
    Copyright &copy Shim-Bi Labs, <?php echo date('Y'); ?>
  </center>
</span>
</div>
</div>
</div>
</div>
          <script type='text/javascript'>
      $("#btnclose").click(function() {
       
       parent.jQuery.fancybox.close();
       
      });
    </script>
  </body>
</html>