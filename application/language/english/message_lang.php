<?php
$lang['NOTIFICATIONS_PERIODICITY'] = array(
    '' => 'Please Select',
    1 => 'Daily',
    2 => 'Every Monday',
    3 => 'Every Tuesday',
    4 => 'Every Wednesday',
    5 =>'Every Thursday',
    6 => 'Every Friday',
    7 => 'Every Saturday',
    8 => 'Every Sunday',
    9 => 'Weekly',
    10 => 'Monthly',
    11 => 'Quarterly',
    12 => 'Semiannually',
    13 => 'Annually',
);

$lang['ACTIVE_INACTIVE_ARR']=array(
								'1'=>'Active',
								'0'=>'In-active',
							  );

$lang['SAVE']='Save';	
$lang['BACK']='Back';
$lang['NEXT']='Next';
$lang['LAST']='>>';
$lang['PREV']='Prev';
$lang['SEARCH']='Search';
$lang['as']='Data Successfully Inserted';
$lang['af']='Data insert failed';
$lang['ADD_SUCCESS'] = 'Record saved successfully!';
$lang['ADD_FAILED'] = 'Record not saved!';
$lang['EDIT_SUCCESS'] = 'Record updated successfully!';
$lang['EDIT_FAILED'] = 'Record updated failed!';
$lang['DELETE_SUCCESS'] = 'Record deleted successfully!';
$lang['ALREADY_EXIST'] = 'User already assign to other case!';


							  
?>
