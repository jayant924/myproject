<?php

/*
 Author  		: Jayant Singh
 Email          : jayantsingh924@gmail.com
 Start Date 	: 9 April 2018
 Last Modified 	: 31 May 2018
 File Name 		: auth.php 
 Purpose 		: Used for CRUD operation,validation,Assign user to case,.			 
*/


class Auth extends CI_controller {

	public function __construct() {

		parent::__construct();
		
		session_start();
		$this->load->model('queries','',TRUE);
        $this->lang->load('message', 'english');
        $this->load->library('pagination');
     }

	

	public function index() {

		
		$this->load->view('files/login');
	}

	


	public function check_login(){


		$this->load->library('form_validation');

	
		$this->form_validation->set_rules('email', 'Email', 'xss_clean|trim|required|valid_email|callback_exists_in');
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[12]');

		if ($this->form_validation->run() == FALSE)
		{
			  $this->load->view('files/login');
		}
		else
		{
		
	
	
		
		if(isset($_POST['email'])){
			
		    $user_data = $this->queries->check_login($_POST['email'],md5($_POST['password']));


            

		    if(!empty($user_data)){
		    	//$this->session->user_data = $user_data;
		    	 $_SESSION['ad_uid']= $user_data['id']; 
		    	 $_SESSION['ad_uname']= $user_data['email']; 

                
               


		    	redirect('auth/profile');
		    }else{
		    	
		    	redirect('auth');
		    }	
		}
	}
}
	
public function profile($rowno = 0)
   {
    // if (empty($_SESSION['ad_uid']))
    //   {
    //     redirect('auth');
    //   }
  
    $search_text = "";
    if ($this->input->post('submit') != NULL)
        {
        $search_text = $this->input->post('search');
        $_SESSION["search"] = @$search_text;
        }
      else
        {

        }
    $order = '';
    $column = '';
    if (isset($_SESSION['col_name']))
        {
        $order = $_SESSION['col_sort'];
        $column = $_SESSION['col_name'];
        }
    $rowperpage = 5;
    if ($rowno != 0)
      {
          $rowno = ($rowno - 1) * $rowperpage;
      }
    $allcount = $this->queries->getrecordCounttest($search_text);
    $users_record = $this->queries->getDatatest($rowno, $rowperpage, $search_text, $order, $column);
    $config['base_url'] = base_url() . 'auth/profile';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $allcount;
    $config['per_page'] = $rowperpage;
    $config['per_page'] = 5;
    $config['num_links'] = 5;
    $this->pagination->initialize($config);
    $posts['pagination'] = $this->pagination->create_links();
    $posts['result'] = $users_record;
    $posts['row'] = @$rowno;
    $posts['search'] = $search_text;
    $posts['total_rows'] = $allcount;
  
    $this->load->view('files/testpage', $posts);
}

public function logout()
{
  session_destroy();  
  redirect('auth');
}

public function dump_session()
{
 echo'<pre>';
 print_r($_SESSION);
 echo '</pre>';
 die();
}

public function exists_in($email)
{
  echo $email;
       $query = $this->db->get_where('myguests', array('email' => $email)); 

                if ($query->num_rows() == 0 )
                {
                        $this->form_validation->set_message('exists_in', 'Please enter an existing email');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }






// --------------------------------- Shorting Function 

public function sorting()

    {
        $field=$_REQUEST['field'];
        $order=$_REQUEST['order'];
    
        $columns = array(
                            0 => 'firstname',
                            1 => 'email',
                            2 => 'reg_date'
                        );

        $_SESSION['col_name'] = $field;
        $_SESSION['col_sort'] = $order;

        

        redirect('auth/profile');
    }
   

    public function remove(){         

            
				if (isset($_GET['id']) && is_numeric($_GET['id']))
				{
				    // get the 'id' variable from the URL
				    $id = $_GET['id'];
				    $name = "SELECT firstname FROM userfile WHERE id=$id";
				    $name=mysql_fetch_assoc($name);
				    $name=$name['name'];

				    if ($stmt = $con->prepare("DELETE FROM userfile WHERE id = $id LIMIT 1"))
				    {
				        $stmt->bind_param("id",$id);
				        unlink("../upload/" . $name);

				        $stmt->execute();
				        $stmt->close();
				    }

				    else
				    {
				        echo "ERROR: could not prepare SQL statement.";
				    }
				    $con->close();

				    // redirect user after delete is successful
				    header("Location: images_delete.php");
				}
				}

   
 }


 
?>