 
 
 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 Author     : Jayant Singh
 Email          : jayantsingh924@gmail.com
 Start Date   : 9 April 2018
 Last Modified  : 31 May 2018
 File Name    : auth.php 
 Purpose    : Used for CRUD operation,validation,Assign user to case,.       
*/

class Welcome extends CI_Controller {



    // Constructor function : Called by default
    public function __construct(){
       
        parent::__construct();
    
        session_start();
        $this->load->model('queries','',TRUE);
        $this->lang->load('message', 'english');
        $this->load->library('pagination');
        $this->load->library('excel');


        $this->load->library('zip');



     }
	

	public function home(){

		$this->load->view('files/home');
	}

  

		public function ut(){

		   $this->load->view('files/addedit');
    }   

     
     public function My_profile(){

     	$this->load->view('files/extra');
     }


     public function table(){

      $this->load->view('files/datatable');
     }





      public function add(){


           $data['countries'] = $this->queries->getcountry();

          
          $post = $this->input->post();

     

        
         if($this->input->post('flag')=='add')
   
          // For file upload

          $file = $this->upload_file();
        

          $file2 = $this->upload_PDF();

          $file3 = $this->upload_exel();
        

          {
            
            $this->form_validation->set_rules('firstname', 'First Names', 'required');
            $this->form_validation->set_rules('lastname', 'Last Names', 'required');
            $this->form_validation->set_rules('email', 'Email','required|callback_exists');
          
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

    
            if($this->form_validation->run()===TRUE)
            {
                
                // create array to insert data
                $insert_data['firstname']=$post['firstname'];
                $insert_data['lastname']=$post['lastname'];
                $insert_data['email']=$post['email'];
                $insert_data['status']=$post['status'];
                $insert_data['password']=md5($post['password']);
                $insert_data['userfile']=$file['file_name'];
                

                $insert_data['userimg']=$file2['file_name2'];
                $insert_data['userexcel']=$file3['file_name3'];
                $insert_data['aboutme']=$post['aboutme'];
                $insert_data['country']=$post['country'];
                $insert_data['state']=$post['state'];
                $insert_data['city']=$post['city'];

                $insert_data['hobbies']=implode(",", $post['hobbies']);

                $insert_data['comment']=$post['comment'];
                $insert_data['dateofbirth']=date("Y-m-d",strtotime($post['dateofbirth']));
                 

                

                 $subject =  "Confirm Registration";
                 $message =  "<b>Dear Sir/Madam ,</b>
                            <br>
                              
                              your registration is Successfully Done with Shimbi Technology lab .
                            
                            <br>
                                   ".$post['file_name']."  <br>    
                         username :".$post['email'].". <br>
                         Password :".$post['password']; 

              // Fixed default values as log
              // $insert_data['added_date']=date('Y-m-d H:i:s');
              // $insert_data['modified_date']=date('Y-m-d H:i:s');
               
                // Pass data to model to insert data in table
                $result=$this->queries->addtest($insert_data);
                  //      $this->send_mail($subject,$message);
            
               //   echo $this->db->last_query();


                
                // redirect to list page
                if($result)
                {
                    redirect('/auth/profile/as');
                }
                else
                {
                    redirect('/auth/profile/af');
                }
            }
        }
        
        $data['post']=$post;
        $data['flag']='add';
        $data['form_action']='/Welcome/add';
      
   
        
        // Load view file and pass $data array to access in view file :
       $data['viewPage']='files/addedit';

                  //   Send data to mail
                          
               
                                    
           
           $this->load->view('comming', $data);
       

    }

    //function to check is newfile upload else save oldfile
  function flagcheck($oldFile, $newFile, $status)
  {
   
    
    if ($newFile) return $newFile;
    elseif ($status == 0)
    {
      return NULL;
    }
    else return $oldFile;
  }
    
    


    function edit($id){

        


       
       $data['countries'] = $this->queries->getcountry();
        // Get data from table using model and passing $id as parameter
        $post = $this->queries->getdata($id);
        $file = $this->upload_file($id);
        $file2 = $this->upload_PDF($id);


        $file3 = $this->upload_exel($id);
        $flagExcel = $this->input->post('flagExcel');
        $flagPdf = $this->input->post('flagPdf');
        $flagImg = $this->input->post('flagImg');

      
      $checkfile = $this->queries->checkFileExist($id, 'pdf');
      $checkimg = $this->queries->checkFileExist($id, 'img');
      $checkexcel = $this->queries->checkFileExist($id, 'excel');
 
      $imageFile = $this->flagcheck($checkimg, isset($file2['file_name']) ? $file2['file_name'] : NULL, $flagImg);
       $pdfFile = $this->flagcheck($checkfile, isset($file['file_name']) ? $file['file_name'] : NULL, $flagPdf);
 $excelFile = $this->flagcheck($checkexcel, isset($file3['file_name']) ? $file3['file_name'] : NULL, $flagExcel);
   



     //   $data['countries'] = $this->queries->getcountry();
       
        if($this->input->post('flag')=='edit')
        {
            $post = $this->input->post();


            
            $this->form_validation->set_rules('firstname', 'First Names', 'required');
            $this->form_validation->set_rules('lastname', 'Last Names', 'required');
            $this->form_validation->set_rules('email', 'E-mail', 'required|callback_new_email['.$id.']');
             $this->form_validation->set_rules('dateofbirth', 'DateOfBirth', 'required');
            
            $this->form_validation->set_rules('status', 'Status', 'required');
        

    
            if($this->form_validation->run()===TRUE)
            {
                // create array to update data


            
                $update_data['firstname']=$post['firstname'];
                $update_data['lastname']=$post['lastname'];
                $update_data['email']=$post['email'];
                $update_data['status']=$post['status'];
                $update_data['userfile']=$pdfFile;
                $update_data['userexcel'] =$excelFile; 
                $update_data['userimg'] =$imageFile;
                $update_data['hobbies'] =implode(",", $post['hobbies']);
                $update_data['comment'] =$post['comment'];
                $update_data['dateofbirth']=date("Y-m-d",strtotime($post['dateofbirth']));
                $update_data['country'] =$post['country'];
                $update_data['state']=$post['state'];
                $update_data['city']=$post['city'];
                $update_data['aboutme']=$post['aboutme'];

          
               

    
                // Fixed default values as log
                //$update_data['modified_date']=date('Y-m-d H:i:s');
                
                // Pass data to model to update data in table using $id


                 $subject =  "Confirm Registration";
                 $message =  "<b>Dear Sir/Madam ,</b>
                            <br>
                              
                              your registration is Successfully Done with Shimbi Technology lab .
                            
                            <br>
                                    ".$post['file_name']."  <br>    
                         username :".$post['email'].". <br>
                         Password :".$post['password']."  "; 






                $result=$this->queries->updatedata($id, $update_data);
                 // $this->send_mail($subject,$message);


                
                // redirect to list page
                if($this->db->affected_rows() > 0)
                {
                    redirect('/auth/profile/es');

                    
                }
                else
                {
                    redirect('/auth/profile/ef');
                }
            }
        }

        $data['post']=$post;
        $data['flag'] = 'edit';
        $data['form_action']='/welcome/edit/'.$id;
        $data['viewPage']='files/addedit';
        // print_r($data);
        // die();
         
         $this->load->view('files/addedit', $data);
    }



function delete($id)
    {
    	
       $this->queries->deletedata($id);
          redirect('/auth/profile/ds');
    }
    
  

 

// callback validation  start

   function email_check($str)
        {
                if ($str == 'jayantsingh924@gmail.com')
                {
                        $this->form_validation->set_message('email_check', 'The E-mail can not be the word "jayantsingh924@gmail.com"');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }



//-------------------------------------------------------------------------------------------//
     
 function exists($email)
        {

  
                $query = $this->db->get_where('myguests', array('email' => $email)); 

                if ($query->num_rows() != 0 )
                {
                        $this->form_validation->set_message('exists', 'Please enter another email');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }





     function new_email($email,$id=NULL)
        
        {
                $this->db->select('email,id');
                $this->db->from('myguests');
                $this->db->where('email',$email);
            
                  $query = $this->db->where_not_in('id',$id);

               
         
                 if($query->get()->num_rows() != 0)
                  {
                     
                        $this->form_validation->set_message('new_email', 'This E-mail is already registerd');
                        return FALSE;
                    
                  }

                  else
                  {
                    return true;
                  }
            
        }
 
//    Call Back Validation End





//    Function for sending mail                             



    
 function  send_mail($subject,$message)
        {

             $this->load->library('email');
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['wordwrap'] = 'TRUE';
        $config['mailtype']='html';
        $config['priority']='1';
        $config['crlf']="\r\n";
        $config['smtpsecure']='ssl';
        $config['auth']=TRUE;
        $config['helo']=NULL;
        $config['protocol'] = 'smtp'; // mail, sendmail, or smtp    The mail sending protocol.
        $config['smtp_host'] ='mail.pipeline.shimbicart.com'; //$smtp_host; // SMTP Server Address.
        $config['smtp_user'] ='alerts@pipeline.shimbicart.com'; //$smtp_user; // SMTP Username.
        $config['smtp_pass'] ='h5KCs6tk8z';   //$smtp_pass; // SMTP Password.
        $config['smtp_port'] ='2525'; //$smtp_port;
        $config['smtp_auth'] = 'TRUE';
       // $subject = 'E-mail Testing';
       // $message = 'hiii';
        $this->email->clear(TRUE); // Clear any uploaded Attachment
        $this->email->initialize($config);
        $this->email->to('jayantsingh924@gmail.com');
            
       
        $this->email->from('alerts@pipeline.shimbicart.com');
        $this->email->subject($subject);

        $this->email->message($message);
        //$this->email->attach('\uploads\email.jpg');
    
      if($this->email->send())
      {
        echo "Success";
      }
      else
      {
        echo $this->email->print_debugger();
      }
    

}



//     function for uploadin file 


  function upload_file()
        {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|xls';
               
                $this->load->library('upload', $config);


                if ( ! $this->upload->do_upload('userfile'))
                {
                        
                    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

                    $error = array('error' => $this->upload->display_errors());
                  


                  //  $this->load->view('files/addedit', $error);
                }
                else
                {
                    return $this->upload->data();
                    

                 //   $this->load->view('files/success', $data);
                    
                }


        }



//     function for uploadin file 


  function upload_PDF()
        {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|xls';
               
                $this->load->library('upload', $config);
               


                if ( ! $this->upload->do_upload('userimg'))
                {
                        
                    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

                    $error = array('error' => $this->upload->display_errors());
                 
                
                  //  $this->load->view('files/addedit', $error);
                }
                else
                {
                     return $this->upload->data();
                   



                   
                   

                 //   $this->load->view('files/success', $data);

                    
                }


        }

function upload_exel()
        {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'xls';
               
                $this->load->library('upload', $config);


                if ( ! $this->upload->do_upload('userexcel'))
                {
                        
                    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

                    $error = array('error' => $this->upload->display_errors());
                  


                  //  $this->load->view('files/addedit', $error);
                }
                else
                {
                    return $this->upload->data();
                    

                 //   $this->load->view('files/success', $data);
                    
                }


        }


function test()
{
    $this->load->view('files/myform');
}






  // ajax function to get country state

  function get_country_state()
  {
    $country_id=$this->input->post('country_id');
    
    $string='no_data';
    
    if(!empty($country_id))
    {
    
      $state_arr=$this->queries->get_state($country_id);
      
      $string= json_encode($state_arr);
      
    }
    echo $string;
    exit();
  }

   function get_state_city()
  {
    $state_id=$this->input->post('state_id');
    
    $string='no_data';
    
    if(!empty($state_id))
    {
    
      $state_arr=$this->queries->get_city($state_id);
      
      $string= json_encode($state_arr);
      
    }
    echo $string;
    exit();
  }



//............................Export in excel  


function export_to_excel_new()
   
   {

   // get all users in array formate
        
        $users = $this->queries->getPosts();

       /*echo'<pre>';
        print_r($users['firstname']);
        die();

*/    $Collumn = array('NAME','E-MAIL','REGISTER DATE','GENDER','DATE OF BIRTH');

        array_unshift($users,$Collumn);
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($users);
 
        $filename='Export.xls'; //save our workbook as this file name
        
        ob_end_clean();

        header('Content-Type: application/vnd.ms-excel'); //mime type
 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
 
        header('Cache-Control: max-age=0'); //no cache
                    

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format
 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
 
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
  
}



// ....................... For generate pdf


  function generatepdf($id)
  

  {


    require_once (APPPATH . 'third_party/tcpdf_6_2_12/tcpdf.php');
      

     // include_once(realpath($_SERVER['DOCUMENT_ROOT']).'/tcpdf_6_2_12/examples/example_001.php');
       //  $tmphtml=$this->load->view('files/pdfview',$vewdata,TRUE);
           //    sample($tmphtml,$path.$filenamechk.'.pdf',NULL,$pass_str);

    $data = $this->queries->getdata($id);
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Pamac');
    $pdf->SetTitle('TCPDF Example 050');
    $pdf->SetSubject('Shimbi Lab Tranning');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->setHeaderFont(Array(
      PDF_FONT_NAME_MAIN,
      '',
      PDF_FONT_SIZE_MAIN
    ));
    $pdf->setFooterFont(Array(
      PDF_FONT_NAME_DATA,
      '',
      PDF_FONT_SIZE_DATA
    ));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if (@file_exists(dirname(__FILE__) . '/lang/eng.php'))
    {
      require_once (dirname(__FILE__) . '/lang/eng.php');
      $pdf->setLanguageArray($l);
    }

    // set default header data
 $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Shimbi Computing Laboratories Pvt. Ltd','Viman Nagar,PUNE', array(0,64,255), array(0,64,128));
         
         $pdf->setFooterData(array(0,64,0), array(0,64,128));

    // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set text shadow effect
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = <<<EOD


<br>
<br>
<h1>Hello Mr. {$data['firstname']} {$data['lastname']} </h1>
<h4 style="color:blue">Here is your all info.</h4>
Email address : {$data['email']} <br>
Register at : {$data['reg_date']}  <br>

Date of Birth : {$data['dateofbirth']}  <br>


Gender : {$data['status']}  <br>
Company : {$data['aboutme']}

EOD;

// Image example with resizing
$pdf->Image(base_url() .'images/testing.png', 15, 140, 150, 100);


// ---------------------------------------------------------
      
    $pdf->SetFont('helvetica', '', 10);
   // $pdf->AddPage();
   // $html = $this->load->view('files/pdfview', $data, TRUE);
   // $pdf->Image(base_url() .'images/testing.png');
    $pdf->WriteHTML($html);
    ob_clean();
    $pdf->SetProtection(array() , "pamac");
    $pdf->Output('', 'I');
  }




// .......................Zip file For Download



 function zipdownload($id)
       
 {
    
    $files = $this->queries->getdata($id);
    
       if (!empty($files))
        {
          if (!is_dir(UPLOAD_ZIP_PATH))
            {
              mkdir(UPLOAD_ZIP_PATH, 0777);
              echo 'folder created';
            }

          $zipFile = UPLOAD_ZIP_PATH . $id . '.zip';
          $zip = new ZipArchive();
          $zip->open($zipFile, ZipArchive::CREATE);
    

      foreach($files as $f)
      {echo $f;
        if ($f != '')
        {
          $ext = pathinfo($f, PATHINFO_EXTENSION);
          $path = realpath(UPLOAD_PDF_EXCEL_PATH . $f);
          if ($ext == 'jpg' || $ext == 'png') $path = realpath(UPLOAD_IMAGE_PATH . $f);
          $zip->addFile($path, $f);
        }
      }
      

      $fcount = $zip->numFiles;
      $zip->close();
      if ($fcount > 0)
      {
        $file_name = basename($zipFile);
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Length: " . filesize($zipFile));
        ob_clean();
        readfile($zipFile);
        exit;
      }
      else
      {
      
      $this->load->view('include/optional');

       // echo '<center><h1 style="font-familly:Trebuchet MS; color:blue;">No File uploaded by user</h1>';
      }
    }
        }


// Function to toggle status of user
  function changeStatus($id=0,$status=null)
  {

    $id = $this->input->post('id');
    $status = $this->input->post('status');
    $t = $this->queries->changeUserStatus($id, $status);
    echo json_encode($t);
 
  }

  // function to view change password page in fancybox
  function changePassword($id)
  {
  

    $post['id'] = $id;


    $this->load->view('include/changepass', $post);
  }
  
//function to change password in fancybox
  function change($id)
  {
    

    $check = $this->input->post('checkpass');
    $new = $this->input->post('newpass');
    $this->load->library('form_validation');
    $this->form_validation->set_rules('newpass', 'Password', 'trim|required|min_length[5]', 'required');
    $this->form_validation->set_rules('checkpass', 'Password Confirmation', 'trim|required|matches[newpass]');
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    if ($this->form_validation->run() == FALSE)
    {
      $post['id'] = $id;
      return $this->load->view('include/changepass', $post);
    }

    $post['password'] = md5($new);
    $this->queries->updatedata($id, $post);
    echo $this->db->last_query();
    $this->load->view('include/success'); 
  }



//function as index function for assign case operation
  function loadAssign($flag = 'add', $message = '')
  {
    $success_message = '';
    $failed_message = '';
    if ($message == 'as')
    {
      $success_message = $this->lang->line('ADD_SUCCESS');
    }
    elseif ($message == 'af')
    {
      $failed_message = $this->lang->line('ALREADY_EXIST');
    }
    elseif ($message == 'es')
    {
      $success_message = $this->lang->line('EDIT_SUCCESS');
    }
    elseif ($message == 'ef')
    {
      $failed_message = $this->lang->line('EDIT_FAILED');
    }
    elseif ($message == 'ds')
    {
      $success_message = $this->lang->line('DELETE_SUCCESS');
    }

    $i = 1;
    ini_set('memory_limit', '-1');
    set_time_limit(500);
    $emplist = $this->queries->getActiveEmployees();
    

    $result = $this->getList($emplist);

    $getCases = $this->queries->getCases();



    $get_var = $this->getCountryStateCityTree();
    if (!$getCases) $getCases = 'Records not found';
    $checkCountry = 0;
    $json = array();
    $data['heading'] = 'Assign case to employee';
    $data['success_message'] = $success_message;
    $data['failed_message'] = $failed_message;
    $data['countrylist'] = $get_var;
    $data['selected'] = isset($data['selected']) ? $data['selected'] : '';
    $data['empList'] = $result;
    $data['posts'] = $getCases;
    ini_set('memory_limit', '-1');
    $data['flag'] = $flag;
    $data['records'] = $getCases;
    $this->load->view('files/assign', $data);
  }



//function to get user name alongwith id for case assign operation
  function getList($array)
  {
    if (!is_array($array))
    {
      return FALSE;
    }

    $result = array();

    foreach($array as $key => $value)
    {
      $result[$value['id']] = $value['firstname'];
    }

    return $result;
  }


//function to get country,state and city tree in fancytree
  function getCountryStateCityTree()
  {
    $countrylist = $this->queries->getCountryStateCity();
    $tree_array = array();
    $prev_zone_id = 0;
    $prev_center_id = 0;
    $prev_subcenter_id = 0;
    foreach($countrylist as $k => $v)
    {
      if ($prev_zone_id != $v['country_id'])
      {
        $tree_array[$v['country_id']] = array(
          'title' => $v['country'],
          'key' => $v['country_id']
        );
        if ($v['state_id'] != "")
        {
          $tree_array[$v['country_id']]['children'][$v['state_id']] = array(
            'title' => $v['state'],
            'key' => $v['country_id'] . '_' . $v['state_id']
          );
          $prev_center_id = $v['state_id'];
          if ($v['city_id'] != "")
          {
            $tree_array[$v['country_id']]['children'][$v['state_id']]['children'][$v['city_id']] = array(
              'title' => $v['city'],
              'key' => $v['country_id'] . '_' . $v['state_id'] . '_' . $v['city_id']
            );
            $prev_subcenter_id = $v['city_id'];
          }
        }

        $prev_zone_id = $v['country_id'];
      }
      elseif ($prev_center_id != $v['state_id'])
      {
        $prev_subcenter_id = 0;
        $tree_array[$v['country_id']]['children'][$v['state_id']] = array(
          'title' => $v['state'],
          'key' => $v['country_id'] . '_' . $v['state_id']
        );
        if ($v['city_id'] != "")
        {
          $tree_array[$v['country_id']]['children'][$v['state_id']]['children'][$v['city']] = array(
            'title' => $v['city'],
            'key' => $v['country_id'] . '_' . $v['state_id'] . '_' . $v['city_id']
          );
          $prev_subcenter_id = $v['city'];
        }

        $prev_center_id = $v['state_id'];
      }
      elseif ($v['city_id'] != "")
      {
        $tree_array[$v['country_id']]['children'][$v['state_id']]['children'][$v['city_id']] = array(
          'title' => $v['city'],
          'key' => $v['country_id'] . '_' . $v['state_id'] . '_' . $v['city_id']
        );
        $prev_subcenter_id = $v['city_id'];
      }
    }

    $json_str = '[';
    foreach($tree_array as $zone)
    {
      $json_str.= '{title: "' . $zone['title'] . '", key: "' . $zone['key'] . '"';
      if (is_array(@$zone['children']))
      {
        $json_str.= ',folder:true,children:[';
        foreach($zone['children'] as $center)
        {
          $json_str.= '{title: "' . $center['title'] . '", key: "' . $center['key'] . '"';
          if (is_array(@$center['children']))
          {
            $json_str.= ',folder:true,children:[';
            foreach($center['children'] as $subcenter)
            {
              $json_str.= '{title: "' . $subcenter['title'] . '", key: "' . $subcenter['key'] . '"},';
            }

            $json_str.= ']';
          }

          $json_str.= '},';
        }

        $json_str.= ']';
      }

      $json_str.= '},';
    }

    $json_str.= ']';
    $get_var = $json_str;
    return $get_var;
  }

  //function to display selected values in fancytree to edit assign case information
  function assignEdit($id)
  {
    $tDetails_tree = $this->queries->getAssignedDetails($id); // get all cases with ids of employee

    $tDetails = $this->queries->getCountryStateCity(); // get all country,state and city name and ids
   
    $array_check_tree1 = array();
    foreach($tDetails_tree as $k => $select_val)
    {
      $t_arr[$select_val['country']][($select_val['state'] == "") ? "NULL" : $select_val['state']][($select_val['city'] == "") ? "NULL" : $select_val['city']] = 1;
    }

    $tree_array = array();
    $prev_zone_id = 0;
    $prev_center_id = 0;
    $prev_subcenter_id = 0;

    // echo '<pre>';print_r($tDetails);die();
    // echo '<pre>';print_r($t_arr);die();

    foreach($tDetails as $k => $v)
    {
      if ($prev_zone_id != $v['country_id'])
      {

        // check if cluster exist

        $flag = 'false';
        if (@$t_arr[$v['country_id']]) $flag = 'true';
        $tree_array[$v['country_id']] = array(
          'title' => $v['country'],
          'key' => $v['country_id'],
          'expanded' => $flag,
          'selected' => $flag
        );
        if ($v['state_id'] != "")
        {

          // check if center exist

          $flag = 'false';
          if (@$t_arr[$v['country_id']][$v['state_id']] || @$t_arr[$v['country_id']]["NULL"]) $flag = 'true';
          $tree_array[$v['country_id']]['children'][$v['state_id']] = array(
            'title' => $v['state'],
            'key' => $v['country_id'] . '_' . $v['state_id'],
            'expanded' => $flag,
            'selected' => $flag
          );
          $prev_center_id = $v['state_id'];
          if ($v['city_id'] != "")
          {

            // check if subcenter exist

            $flag = 'false';
            if (@$t_arr[$v['country_id']][$v['state_id']][$v['city_id']] || @$t_arr[$v['country_id']]["NULL"] || @$t_arr[$v['country_id']][$v['state_id']]["NULL"]) $flag = 'true';
            $tree_array[$v['country_id']]['children'][$v['state_id']]['children'][$v['city_id']] = array(
              'title' => $v['city'],
              'key' => $v['country_id'] . '_' . $v['state_id'] . '_' . $v['city_id'],
              'expanded' => $flag,
              'selected' => $flag
            );
            $prev_subcenter_id = $v['city_id'];
          }
        }

        $prev_zone_id = $v['country_id'];
      }
      elseif ($prev_center_id != $v['state_id'])
      {
        $prev_subcenter_id = 0;

        // check if center exist

        $flag = 'false';
        if (@$t_arr[$v['country_id']][$v['state_id']] || @$t_arr[$v['country_id']]["NULL"]) $flag = 'true';
        $tree_array[$v['country_id']]['children'][$v['state_id']] = array(
          'title' => $v['state'],
          'key' => $v['country_id'] . '_' . $v['state_id'],
          'expanded' => $flag,
          'selected' => $flag
        );
        if ($v['city_id'] != "")
        {

          // check if subcenter exist

          $flag = 'false';
          if (@$t_arr[$v['country_id']][$v['state_id']][$v['city_id']] || @$t_arr[$v['country_id']]["NULL"] || @$t_arr[$v['country_id']][$v['state_id']]["NULL"]) $flag = 'true';
          $tree_array[$v['country_id']]['children'][$v['state_id']]['children'][$v['city_id']] = array(
            'title' => $v['city'],
            'key' => $v['country_id'] . '_' . $v['state_id'] . '_' . $v['city_id'],
            'expanded' => $flag,
            'selected' => $flag
          );
          $prev_subcenter_id = $v['city_id'];
        }

        $prev_center_id = $v['state_id'];
      }
      elseif ($v['city_id'] != "")
      {

        // check if subcenter exist

        $flag = 'false';
        if (@$t_arr[$v['country_id']][$v['state_id']][$v['city_id']] || @$t_arr[$v['country_id']]["NULL"] || @$t_arr[$v['country_id']][$v['state_id']]["NULL"]) $flag = 'true';
        $tree_array[$v['country_id']]['children'][$v['state_id']]['children'][$v['city_id']] = array(
          'title' => $v['city'],
          'key' => $v['country_id'] . '_' . $v['state_id'] . '_' . $v['city_id'],
          'expanded' => $flag,
          'selected' => $flag
        );
        $prev_subcenter_id = $v['city_id'];
      }
    }

    $json_str = '[';
    foreach($tree_array as $zone)
    {
      $json_str.= '{title: "' . $zone['title'] . '", key: "' . $zone['key'] . '", expanded: ' . $zone['expanded'] . ', selected: ' . $zone['selected'];
      if (is_array(@$zone['children']))
      {
        $json_str.= ',children:[';
        foreach($zone['children'] as $center)
        {
          $json_str.= '{title: "' . $center['title'] . '", key: "' . $center['key'] . '", expanded: ' . $center['expanded'] . ', selected: ' . $center['selected'];
          if (is_array(@$center['children']))
          {
            $json_str.= ',children:[';
            foreach($center['children'] as $subcenter)
            {
              $json_str.= '{title: "' . $subcenter['title'] . '", key: "' . $subcenter['key'] . '", expanded: ' . $subcenter['expanded'] . ', selected: ' . $subcenter['selected'] . '},';
            }

            $json_str.= ']';
          }

          $json_str.= '},';
        }

        $json_str.= ']';
      } 

      $json_str.= '},';
    }   //foreach for $tree_array close

    $json_str.= ']';
    $get_var = $json_str;
    $emplist = $this->queries->getActiveEmployees();
    $result = $this->getList($emplist);
    $data['flag'] = 'edit';
    $data['heading'] = 'Edit case details';
    $data['countrylist'] = $get_var;
    $data['empList'] = $result;
    $data['selected'] = $id;
  


    $this->load->view('files/assign', $data);
  
  
  }

//function to delete all assigned case record of user 
  function assignDelete($id)
  {
    $this->queries->assignDelete($id);
    redirect('welcome/loadAssign/0/ds');
  }

//function to update case assign details
  function editUpdate()
  {
    $id = $this->input->post('empList');
    $this->queries->assignDelete($id);
    $this->saveCase('edit');
    redirect('welcome/loadAssign/0/es');
  }

// function to display  case exist with user message page in fancybox
  function recordExist($id)
  {
    $data['id'] = $id;
    $this->load->view('DeleteEmp', $data);
  }

//function to display deleted user message in fancybox
  function deletedSuccess()
  {
    $this->load->view('successDelete');
  }


//function to insert a new case into database
  function saveCase($flag = 0)
  {
    $id = $this->input->post('empList');
    $loc = $this->input->post('ft_1');

    // $checkExist = $this->Queries->userExist($id);
    // if (!$checkExist){
    //  redirect('welcome/loadAssign/0/af');

  
    if (!empty($loc))
    {
      foreach($loc as $l)
      {
        $trim = (explode("_", $l));
        $country = $trim['0'];
        $state = isset($trim['1']) ? $trim['1'] : NULL;
        $city = isset($trim['2']) ? $trim['2'] : NULL;
        $data = array(
          'empid' => $id,
          'city' => $city,
          'state' => $state,
          'country' => $country,
          'created_by' => $_SESSION['ad_uid'],
          'created_at' => date("Y-m-d H:i:s") ,

        ); 


        $this->queries->assignCase($data);
      }

      if ($flag == 0) redirect('welcome/loadAssign/0/as');
    }
    else
    {
      return false;
    }
  }


// Function to generate zip file
  function generateZipnew($id)
  {
    $files = $this->queries->getAllFiles($id);
   
    if (!empty($files))
    {
      if (!is_dir(UPLOAD_ZIP_PATH))
      {
        mkdir(UPLOAD_ZIP_PATH, 0777);
        echo 'folder created';
      }

      $zipFile = UPLOAD_ZIP_PATH . $id . '.zip';

  

      $zip = new ZipArchive();
      $zip->open($zipFile, ZipArchive::CREATE);

      
      foreach($files as $f)
      {
        echo $f;

        if ($f != '')
        {
          $ext = pathinfo($f, PATHINFO_EXTENSION);
          $path = realpath(UPLOAD_PDF_EXCEL_PATH . $f);
          if ($ext == 'jpg' || $ext == 'png') $path = realpath(UPLOAD_IMAGE_PATH . $f);
          $zip->addFile($path, $f);
        }
      }

      $fcount = $zip->numFiles;
     
      $zip->close();
      if ($fcount > 0)
      {
        $file_name = basename($zipFile);
         
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Length: " . filesize($zipFile));
        ob_clean();
        readfile($zipFile);
        exit;
      }
      else
      {
        echo 'No File uploaded by user';
      }
    }
  }
//function for input field validation
  function validation($flag, $id = 0)
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[5]|max_length[25]', 'required');
    $this->form_validation->set_rules('gender', 'Gender', 'required', 'required');
    $this->form_validation->set_rules('city', 'city', 'required|callback_set_msg', 'required');
    $this->form_validation->set_rules('dob_id', 'Date of Birth', 'required', 'required');
    // $this->form_validation->set_rules('myfile','myfile','callback_image_msg');
    if ($flag == 'add')
    {
      $this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email|is_unique[schooldata.username]', 'required');
      $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]', 'required');
      $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
    }
    else
    {
      $this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email|callback_check_username[' . $id . ']', 'required');
    }

    if ($this->form_validation->run() == FALSE)
    {
      return validation_errors();
    }
  }

  

}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */




